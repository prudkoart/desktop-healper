import pyttsx3
import speech_recognition as sr
from fuzzywuzzy import fuzz
import datetime
import webbrowser
import sys

opts = {"cmds":
            {"weather": ("открой погоду", "скажи погоду", "подскажи погоду", 'погода на сегодня',
                         "погода", "погоду"),
             "vk": ("открой вк", 'вконтакте', "вк", "открой вконтакте", "vk"),
             "internet": ("открой гугл", "интернет", 'гугл', "хром"),
             "youtube": ("открой ютуб", "ютуб", "youtube"),
             "ali": ("открой али", "открой алиэкспрес", "али", "алиэкспрес"),
             "ctime": ("текущее время", "который час", "скажи время", "сколько время", "сколько времени"),
             "deals": ("дела", "делишки", 'как сам', 'как дела'),
             "hello": ('привет', 'здравствуй', 'здравствуйте'),
             "stop": ('пока', 'стоп', 'останови', 'остановись')
             }
        }


def speak(what):
    print(what)
    speak_engine = pyttsx3.init()
    speak_engine.say(what)
    speak_engine.runAndWait()
    speak_engine.stop()


def callback(recognizer, audio):
    try:
        voice = recognizer.recognize_google(audio, language="ru-RU").lower()
        print("[log] Распознано: " + voice)
        cmd = voice

        # распознаем и выполняем команду
        cmd = recognize_cmd(cmd)
        execute_cmd(cmd['cmd'])

    except sr.UnknownValueError:
        print("[log] Голос не распознан!")
    except sr.RequestError:
        print("[log] Неизвестная ошибка, проверьте интернет!")


def recognize_cmd(cmd):
    RC = {'cmd': '', 'percent': 0}
    for c, v in opts['cmds'].items():
        for x in v:
            vrt = fuzz.ratio(cmd, x)
            if vrt > RC['percent']:
                RC['cmd'] = c
                RC['percent'] = vrt
    return RC


def execute_cmd(cmd):
    if cmd == 'ctime':
        now = datetime.datetime.now()
        speak("Сейчас " + str(now.hour) + ":" + str(now.minute))
    elif cmd == 'deals':
        speak("Пока отлично.")
    elif cmd == 'weather':
        webbrowser.open_new_tab('https://www.gismeteo.ru/')
    elif cmd == 'vk':
        webbrowser.open_new_tab('https://vk.com')
    elif cmd == 'youtube':
        webbrowser.open_new_tab('https://www.youtube.com/')
    elif cmd == 'internet':
        webbrowser.open_new_tab('http://google.com')
    elif cmd == 'ali':
        webbrowser.open_new_tab('https://ru.aliexpress.com')
    elif cmd == 'hello':
        now = datetime.datetime.now()

        if 6 <= now.hour < 12:
            speak("Доброе утро!")
            speak("Чем могу помочь?")
        elif 12 <= now.hour < 18:
            speak("Добрый день!")
            speak("Чем могу помочь?")
        elif 18 <= now.hour < 23:
            speak("Добрый вечер!")
            speak("Чем могу помочь?")
        else:
            speak("Доброй ночи!")
            speak("Чем могу помочь?")
    elif cmd == 'stop':
        speak("До свидания")
        sys.exit()
    else:
        print("Команда не распознана!")
