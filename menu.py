import webbrowser
import keyboard
import speech_recognition as sr
from functions import callback
import csv
from PyQt5 import QtCore, QtGui, QtWidgets
import sys


class Ui_mainWindow(object):

    def setupUi(self, mainWindow):
        mainWindow.setObjectName("mainWindow")

        mainWindow.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.FramelessWindowHint)
        self.pixmap = QtGui.QPixmap("bob.png")
        try:
            with open("data.csv", encoding='utf-8') as r_file:
                file_reader = csv.DictReader(r_file, delimiter="|")
                for row in file_reader:
                    mainWindow.resize(int(row['length']), int(row['height']))
                    self.pixmap_resized = self.pixmap.scaled(int(row['length']),
                                                             int(row['height']),
                                                             QtCore.Qt.KeepAspectRatio,
                                                             QtCore.Qt.SmoothTransformation)
        except:
            mainWindow.resize(400, 556)
            self.pixmap_resized = QtGui.QPixmap("bob.png")

        self.pal = mainWindow.palette()
        self.pal.setBrush(QtGui.QPalette.Normal, QtGui.QPalette.Window,
                          QtGui.QBrush(self.pixmap_resized))
        self.pal.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window,
                          QtGui.QBrush(self.pixmap_resized))
        mainWindow.setPalette(self.pal)
        mainWindow.setMask(self.pixmap_resized.mask())
        mainWindow.setStyleSheet("#pushButton\n"
                                 "{\n"
                                 "    background-image: url(bob.png);\n"
                                 "    background-repeat: norepeat;\n"
                                 "}")
        self.centralwidget = QtWidgets.QWidget(mainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(0, 0, 721, 601))
        self.pushButton.setText("")
        self.pushButton.setObjectName("pushButton")
        mainWindow.setCentralWidget(self.centralwidget)
        self.retranslateUi(mainWindow)
        QtCore.QMetaObject.connectSlotsByName(mainWindow)

    def retranslateUi(self, mainWindow):
        pass


class Ui_VoiceWindow(QtWidgets.QDialog):
    def __init__(self, VoiceWindow, parent=None):
        super(Ui_VoiceWindow, self).__init__(parent)

        self.retranslateUi(VoiceWindow)
        QtCore.QMetaObject.connectSlotsByName(VoiceWindow)

    def retranslateUi(self, MenuWindow):
        _translate = QtCore.QCoreApplication.translate
        MenuWindow.setWindowTitle(_translate("MenuWindow", "MainWindow"))


class Ui_MenuWindow(QtWidgets.QDialog):
    def __init__(self, MenuWindow, parent=None):
        super(Ui_MenuWindow, self).__init__(parent)

        self.SettingsButton = QtWidgets.QPushButton(self)
        self.SettingsButton.setGeometry(QtCore.QRect(0, 0, 281, 51))
        self.SettingsButton.setObjectName("SettingsButton")
        self.TreyButton = QtWidgets.QPushButton(self)
        self.TreyButton.setGeometry(QtCore.QRect(0, 50, 281, 51))
        self.TreyButton.setObjectName("TreyButton")
        self.VoiceButton = QtWidgets.QPushButton(self)
        self.VoiceButton.setGeometry(QtCore.QRect(0, 100, 281, 51))
        self.VoiceButton.setObjectName("VoiceButton")
        self.ExitButton = QtWidgets.QPushButton(self)
        self.ExitButton.setGeometry(QtCore.QRect(0, 150, 281, 51))
        self.ExitButton.setObjectName("ExitButton")

        self.retranslateUi(MenuWindow)
        QtCore.QMetaObject.connectSlotsByName(MenuWindow)

    def retranslateUi(self, MenuWindow):
        _translate = QtCore.QCoreApplication.translate
        MenuWindow.setWindowTitle(_translate("MenuWindow", "MenuWindow"))
        self.SettingsButton.setText(_translate("MenuWindow", "Настройки"))
        self.TreyButton.setText(_translate("MenuWindow", "Скрыть в трей"))
        self.VoiceButton.setText(_translate("MenuWindow", "Голосовое управление"))
        self.ExitButton.setText(_translate("MenuWindow", "Выход"))


class MyWin(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        self.ui = Ui_mainWindow()  # Экземпляр класса Ui_MainWindow, в нем конструктор всего GUI.
        self.ui.setupUi(self)  # Инициализация GUI
        keyboard.add_hotkey(keyboard.KEY_DOWN, lambda: webbrowser.get().open("https://youtu.be/dQw4w9WgXcQ"))
        keyboard.add_hotkey(keyboard.KEY_UP, lambda: webbrowser.get().open("https://www.gismeteo.ru/"))
        keyboard.add_hotkey('ctrl+s', lambda: self.show())
        keyboard.add_hotkey('ctrl+h', lambda: self.hide())
        self.ui.pushButton.clicked.connect(self.openMenu)  # Открыть новую форму

    def openMenu(self):
        dialog = Ui_MenuWindow(self)
        dialog.TreyButton.clicked.connect(self.trey)
        dialog.VoiceButton.clicked.connect(self.Voice)
        dialog.SettingsButton.clicked.connect(self.set)
        dialog.SettingsButton.clicked.connect(dialog.close)
        dialog.ExitButton.clicked.connect(self.Exit)
        dialog.exec_()

    def mousePressEvent(self, event):
        self.setCursor(QtCore.Qt.ClosedHandCursor)
        self.start = self.mapToGlobal(event.pos())
        self.pressing = True

    def mouseMoveEvent(self, event):
        if self.pressing:
            self.end = self.mapToGlobal(event.pos())
            self.movement = self.end - self.start
            self.setGeometry(self.mapToGlobal(self.movement).x(),
                             self.mapToGlobal(self.movement).y(),
                             self.width(),
                             self.height())
            self.start = self.end

    def mouseReleaseEvent(self, QMouseEvent):
        self.pressing = False
        self.unsetCursor()


    def Voice(self):
        voice = Ui_VoiceWindow(self)
        voice.r = sr.Recognizer()
        voice.m = sr.Microphone(device_index=1)

        with voice.m as source:
            voice.r.adjust_for_ambient_noise(source, duration=5)

        voice.r.listen_in_background(voice.m, callback)


    def trey(self):
        self.hide()

    def Exit(self):
        sys.exit()


    def set(self):
        Dialog.show()


class SettingWin(object):
    def __init__(self):
        self.url = QtWidgets.QLineEdit(Dialog)
        self.name = QtWidgets.QLineEdit(Dialog)
        self.label = QtWidgets.QLabel(Dialog)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.Accept = QtWidgets.QPushButton(Dialog)
        self.height_spinbox = QtWidgets.QSpinBox(Dialog)
        self.len_spinbox = QtWidgets.QSpinBox(Dialog)
        self.SaveChange = QtWidgets.QPushButton(Dialog)
        self.len_lable = QtWidgets.QLabel(Dialog)
        self.height_lable = QtWidgets.QLabel(Dialog)

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(309, 277)
        self.name.setGeometry(QtCore.QRect(10, 40, 113, 20))
        self.name.setObjectName("name")
        self.url.setGeometry(QtCore.QRect(10, 90, 113, 20))
        self.url.setObjectName("url")
        self.label.setGeometry(QtCore.QRect(10, 10, 101, 16))
        self.label.setObjectName("label")
        self.label_2.setGeometry(QtCore.QRect(20, 70, 101, 16))
        self.label_2.setObjectName("label_2")
        self.Accept.setGeometry(QtCore.QRect(200, 30, 91, 31))
        self.Accept.setObjectName("Accept")
        self.Accept.clicked.connect(self.add_marker)
        self.height_spinbox.setGeometry(QtCore.QRect(10, 150, 111, 22))
        self.height_spinbox.setObjectName("spinBox")
        self.height_spinbox.setMaximum(9999)
        self.len_spinbox.setMaximum(9999)
        self.len_spinbox.setGeometry(QtCore.QRect(10, 200, 111, 22))
        self.len_spinbox.setObjectName("spinBox_2")
        self.SaveChange.setGeometry(QtCore.QRect(200, 170, 91, 41))
        self.SaveChange.setObjectName("pushButton")
        self.SaveChange.clicked.connect(self.save)
        self.height_lable.setGeometry(QtCore.QRect(10, 130, 101, 16))
        self.height_lable.setObjectName("label_3")
        self.len_lable.setGeometry(QtCore.QRect(10, 180, 101, 16))
        self.len_lable.setObjectName("label_4")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Form"))
        self.label.setText(_translate("Dialog", "Название закладки"))
        self.label_2.setText(_translate("Dialog", "ссылка"))
        self.Accept.setText(_translate("Dialog", "добавить"))
        self.SaveChange.setText(_translate("Dialog", "сохранить\n"
                                                     "измения"))
        self.height_lable.setText(_translate("Dialog", "высота"))
        self.len_lable.setText(_translate("Dialog", "длина"))

    def save(self):
        a = dict(length=self.height_spinbox.value(),
                 height=self.len_spinbox.value())
        print()
        with open("data.csv", mode='w', encoding='utf-8') as w_file:
            file_writer = csv.DictWriter(w_file, delimiter="|",
                                         lineterminator="\r", fieldnames=a.keys())
            file_writer.writeheader()
            file_writer.writerow(a)
        sys.exit()

    def add_marker(self):
        if self.name.text() != '' and self.url.text() != '':
            a = dict(name=self.name.text(),
                     url=self.url.text())
            print()
            with open("markers.csv", mode='w', encoding='utf-8') as w_file:
                file_writer = csv.DictWriter(w_file, delimiter="|",
                                             lineterminator="\r", fieldnames=a.keys())
                file_writer.writeheader()
                file_writer.writerow(a)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyWin()
    Dialog = QtWidgets.QWidget()
    ui = SettingWin()
    ui.setupUi(Dialog)
    window.show()
    sys.exit(app.exec_())
